/*
 *
 * Copyright (c) 2005-2019 Imperas Software Ltd., www.imperas.com
 *
 * The contents of this file are provided under the Software License
 * Agreement that you accepted before downloading this file.
 *
 * This source forms part of the Software and can be used for educational,
 * training, and demonstration purposes but cannot be used for derivative
 * works except in cases where the derivative works require OVP technology
 * to run.
 *
 * For open source models released under licenses that you can use for
 * derivative works, please visit www.OVPworld.org or www.imperas.com
 * for the location of the open source models.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "dmacRegisters.h"


typedef unsigned int  Uns32;
typedef unsigned char Uns8;

#include "riscvInterrupts.h"

#define LOG(_FMT, ...)  printf( "TEST DMA: \n" _FMT,  ## __VA_ARGS__)

void int_init(void (*handler)()) {

    // Set MTVEC register to point to handler function in direct mode
    int handler_int = (int) handler & ~0x1;
    write_csr(mtvec, handler_int);

    // Enable Machine mode external interrupts
    set_csr(mie, MIE_MEIE);
}

void int_enable() {
    set_csr(mstatus, MSTATUS_MIE);
}


static inline void writeReg32(Uns32 address, Uns32 offset, Uns32 value)
{
    *(volatile Uns32*) (address + offset) = value;
}


static inline Uns32 readReg32(Uns32 address, Uns32 offset)
{
    return *(volatile Uns32*) (address + offset);
}

static inline void writeReg8(Uns32 address, Uns32 offset, Uns8 value)
{
    *(volatile Uns8*) (address + offset) = value;
}

static inline Uns8 readReg8(Uns32 address, Uns32 offset)
{
    return *(volatile Uns8*) (address + offset);
}

#define ENABLE      0x00000001
#define INTEN       0x00008000
// burst size is 1<<BURST_SIZE
#define BURST_SIZE       2

volatile static Uns32 interruptCount = 0;

void interruptHandler(void)
{

    LOG("Interrupt\n");
//    // read interrupt status
//    Uns32 intStatus = readReg8(DMA_BASE, DMA_INT_TC_STATUS);
//
//    // check channel 0 interrupts enabled and status indicates interrupt set
//    if ( (readReg32(DMA_BASE, DMA_C0_CONFIGURATION) & 0x8000 ) &&
//         (intStatus & 1<<0)){
//        LOG("Interrupt ch0 0x%x (0x%02x)\n",
//            readReg32(DMA_BASE, DMA_C0_CONFIGURATION), intStatus);
//        // disable ch0 interrupt
//        writeReg32(DMA_BASE, DMA_C0_CONFIGURATION, 0);
//        // clear ch0 interrupt
//        writeReg8(DMA_BASE, DMA_INT_TC_STATUS, 1<<0);
//        interruptCount++;
//    }
//
//    // check channel 1 interrupts enabled and status indicates interrupt set
//    if ( (readReg32(DMA_BASE, DMA_C1_CONFIGURATION) & 0x8000 ) &&
//         (intStatus & 1<<1)){
//        LOG("Interrupt ch1 0x%x (0x%02x)\n",
//            readReg32(DMA_BASE, DMA_C1_CONFIGURATION), intStatus);
//        // disable ch1 interrupt
//        writeReg32(DMA_BASE, DMA_C1_CONFIGURATION, 0);
//        // clear ch1 interrupt
//        writeReg8(DMA_BASE, DMA_INT_TC_STATUS, 1<<1);
//        interruptCount++;
//    }

//if(interruptCount > 2){
      writeReg32(DMA_BASE+BUS1_BASE, (Uns32)(0x10), (Uns32)(1));
//}
}

/*
static void dmaBurst(Uns32 ch, void *from, void *to, Uns32 bytes)
{
    Uns32 offset = ch * DMA_CHANNEL_STRIDE;
    LOG("dmaBurst ch:%d  bytes:%d\n", ch, bytes);
    writeReg32(DMA_BASE, DMA_C0_SRC_ADDR + offset, (Uns32)from);
    writeReg32(DMA_BASE, DMA_C0_DST_ADDR + offset, (Uns32)to);
    writeReg32(DMA_BASE, DMA_C0_CONTROL  + offset, bytes);

    writeReg32(DMA_BASE, DMA_C0_CONFIGURATION + offset, ENABLE|INTEN);
}
*/


int main(int argc, char **argv)
{ 
    printf("Start RISCV1 program\n");


    //while(1);

    //Set execution count = 0
    int count_riscv1 = 0;

    // Prepare test data in ram1
    Uns32 offset_1 = 0x200400;     
    Uns8 value_1   = 0x0;    
    int count_num_1 = 0;
    int count_char_1 = 0;
    int total_count_1 = 0;

    writeReg8(RAM1_BASE+BUS1_BASE, offset_1, value_1);
    total_count_1 += 1;

    while(total_count_1 < 1024){
      if(count_num_1 <= 9 && count_char_1 == 0){
        if(count_num_1 == 9){
          value_1 = 0x41;
          count_char_1 += 1;
        }
        else {
          value_1 += 1;
          count_num_1 += 1;
        }
        offset_1 += 1;
        //printf("count: %d, off: %x, value: %d\n", total_count_1, offset_1, value_1);
        writeReg8(RAM1_BASE+BUS1_BASE, offset_1, value_1);
        total_count_1 += 1;
      }
      else{
        if(count_char_1 == 6){
          // Reset value
          value_1 = 0x0;
          count_num_1 = 0;
          count_char_1 = 0;
        }
        else {
          value_1 += 1;
          count_char_1 += 1;
        }
        offset_1 += 1;
        //printf("count: %d, off: %x, value: %d\n", total_count_1, offset_1, value_1);
        writeReg8(RAM1_BASE+BUS1_BASE, offset_1, value_1);
        total_count_1 += 1;
      }
    }
    printf("RAM1 %d data prepared.\n", total_count_1);
 
    printf("------------- target RAM1 data ------------\n");
    for(int i = 0; i < 16; i++)
    printf("address = %x , mem_data = %d\n",(0x200000+i),*(Uns8*)(RAM1_BASE+BUS1_BASE+0x200000+i));

    writeReg32(RAM1_BASE+BUS1_BASE, 0x200800, 0x0);
    printf("Reset Arbitrator reg to 0\n");
    
    // Launch ISR
    int_init(trap_entry);
    int_enable();

    while (count_riscv1 < 3){

      if(*(Uns32*)(RAM1_BASE+BUS1_BASE+0x200800) == 0 ){
      printf("====================================================\n");
      printf("                 riscv 1 transaction %d\n", count_riscv1+1);
      printf("====================================================\n");
        // Write DMA control registers to move data
        writeReg32(DMA_BASE+BUS1_BASE, (Uns32)(0x0), (Uns32)(RAM1_BASE+0x200400));
        writeReg32(DMA_BASE+BUS1_BASE, (Uns32)(0x4), (Uns32)(RAM6_BASE+0x300000));
        writeReg32(DMA_BASE+BUS1_BASE, (Uns32)(0x8), (Uns32)(0x400));
        writeReg32(DMA_BASE+BUS1_BASE, (Uns32)(0xc), (Uns32)(1));

        wfi();

        count_riscv1+=1;
        
        writeReg32(RAM1_BASE+BUS1_BASE, 0x200800, 0x1);
        printf("RISCV1 Set Arbitrator reg to 1\n");
      }
    }
   
    while( *(Uns32*)(RAM1_BASE+BUS1_BASE+0x200804) != 0x1 ){};
    
    printf("END RISCV1 simulation\n");


    return 1;
}

