#include "systemc.h"
#include "ir_adapter.h"

void IR_ADAPTER::ir_adapter(){
  if(ir_dma.read() == 1) ir.write(1);
  else ir.write(0);
}
