#include "dma.h"
#define debug

void DMA::b_transport(tlm::tlm_generic_payload& trans, sc_time& delay){

  tlm::tlm_command cmd_s  = trans.get_command();
  sc_dt::uint64    addr_s = trans.get_address();
  unsigned char*   data_s = trans.get_data_ptr();
  unsigned int     len    = trans.get_data_length();

  if (cmd_s == tlm::TLM_WRITE_COMMAND) {
#ifdef debug
    cout << "---- DMA write control REGs ----" << endl;
    cout << "addr    : " << addr_s << endl;
    cout << "data    : " << (unsigned int)*data_s << endl;
    cout << "data len: " << len << endl;
#endif
    //DMA Initialization from cpu
    if(!R_START){
      if     (addr_s == SOURCE_ADDR) R_SOURCE = *(reinterpret_cast<unsigned int*>(data_s));
      else if(addr_s == TARGET_ADDR) R_TARGET = *(reinterpret_cast<unsigned int*>(data_s));
      else if(addr_s == SIZE_ADDR)   R_SIZE   = *(reinterpret_cast<unsigned int*>(data_s));
      else if(addr_s == START_ADDR){
        channel = 0;
        R_START  = *(reinterpret_cast<unsigned int*>(data_s));
      }
      else if(addr_s == 0x10){ 
        R_CLEAR  = *(reinterpret_cast<unsigned int*>(data_s));
#ifdef debug
      	cout << "RISCV1 CLEAR : 1 " << endl;
#endif
      }
      else if(addr_s == SOURCE_ADDR + 0x20) R_SOURCE_2 = *(reinterpret_cast<unsigned int*>(data_s));
      else if(addr_s == TARGET_ADDR + 0x20) R_TARGET_2 = *(reinterpret_cast<unsigned int*>(data_s));
      else if(addr_s == SIZE_ADDR   + 0x20)   R_SIZE_2 = *(reinterpret_cast<unsigned int*>(data_s));
      else if(addr_s == START_ADDR  + 0x20){
        channel = 1;
        R_START_2 = *(reinterpret_cast<unsigned int*>(data_s));
      }
      else if(addr_s == 0x10 + 0x20){ 
        R_CLEAR_2  = *(reinterpret_cast<unsigned int*>(data_s));
#ifdef debug
      	cout << "RISCV2 CLEAR : 1 " << endl;
#endif
      }
      if (addr_s == START_ADDR) {
        //DMA read enable
	rw_m = READ;
#ifdef debug
      	cout << endl << "rw is set to WRITE" << endl;
#endif
      } 
    }
  }

  //wait(delay);       //use external delay
  wait(10, SC_NS); //use internal delay
  trans.set_response_status(tlm::TLM_OK_RESPONSE);
}

void DMA::dma() {
  //R_SOURCE = 0;
  //R_TARGET = 0;
  //R_SIZE   = 0;
  //R_START  = 0;
  //data_m   = 0;

  while (1) {
    //wait();

    tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;
    //tlm::tlm_generic_payload trans;
    tlm::tlm_command cmd;
    sc_time delay = sc_time(10, SC_NS);


  if(channel == 0){
    while(R_START == 1 && R_SIZE != 0){
#ifdef debug1
      cout << endl;
      cout << hex << "SOURCE: " << R_SOURCE << endl;
      cout << hex << "TARGET: " << R_TARGET << endl;
      cout << hex << "SIZE  : " << R_SIZE << endl;
      cout << hex << "START : " << R_START << endl;
#endif
      t_addr_temp = R_TARGET;
      s_addr_temp = R_SOURCE;
      // DMA read from source
      if(R_START && rw_m == READ){
#ifdef debug1
	      cout << "--- DMA read data from source memory ! ---" << endl;
#endif         
	      cmd = tlm::TLM_READ_COMMAND;
        trans->set_command(cmd);
        trans->set_address(s_addr_temp);
        trans->set_data_ptr(reinterpret_cast<unsigned char*> (&data_m));
        trans->set_data_length(1);
        trans->set_streaming_width(1);
        trans->set_byte_enable_ptr(0);
        trans->set_dmi_allowed(false);
        trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
        // Sent to DMA slave port
        master_port->b_transport(*trans, delay);

        //reg_m = trans.get_data_ptr();
        //data_m = *reg_m;
        rw_m = WRITE;
#ifdef debug1
        cout << hex  << "SOURCE: " << R_SOURCE << endl;
        cout << hex  << "TARGET: " << R_TARGET << endl;
        cout << hex  << "SIZE  : " << R_SIZE   << endl;
        cout << hex  << "START : " << R_START  << endl;
        cout << "data  : " << reinterpret_cast<unsigned int*> (&data_m) << endl;
      	cout << "---------------" << endl;
#endif
      	break;
      }
      // DMA write to target
      else if(R_START && rw_m == WRITE){
#ifdef debug1
 	      cout << "--- DMA write data to target memory ! ---" << endl;
#endif
	      cmd = tlm::TLM_WRITE_COMMAND;
        trans->set_command(cmd);
        trans->set_address(t_addr_temp);
        trans->set_data_ptr(reinterpret_cast<unsigned char*> (&data_m));
        trans->set_data_length(1);
        trans->set_streaming_width(1);
        trans->set_byte_enable_ptr(0);
        trans->set_dmi_allowed(false);
        trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
        // Sent to DMA slave port
        master_port->b_transport(*trans, delay);
        
       	R_SOURCE = R_SOURCE + 1;
        R_TARGET = R_TARGET + 1;
        rw_m = READ;
        R_SIZE -= 1;
#ifdef debug1
        cout << hex << "SOURCE: " << R_SOURCE << endl;
        cout << hex << "TARGET: " << R_TARGET << endl;
        cout << hex << "SIZE  : " << R_SIZE << endl;
        cout << hex << "START : " << R_START << endl;
        cout << "data  : " << reinterpret_cast<unsigned int*> (&data_m) << endl;
	      cout << "---------------" << endl;
#endif
	      break;
      }
    }// close while(R_START == 1 && R_SIZE != 0)

    if(R_SIZE == 0 && R_START == 1){
#ifdef debug
      cout << "---- Interrupt CPU RISCV1 ----" << endl;
#endif
      R_START = 0;
      ir.write(1);
      ir2.write(1);
    }

    // Clear REG write from plaform 
    if(ir.read() == 1 && R_CLEAR == 1) {
#ifdef debug
      cout << "---- RISCV1 Clear DMA ----" << endl;
#endif
      ir.write(0);
      ir2.write(0);
      R_SOURCE = 0;
      R_TARGET = 0;
      R_SIZE   = 0;
      R_START  = 0;
      R_CLEAR  = 0;
    }
    
   }// close channel == 0
  
  else if(channel == 1){
    while(R_START_2 == 1 && R_SIZE_2 != 0){
#ifdef debug1
      cout << endl;
      cout << hex << "SOURCE: " << R_SOURCE_2 << endl;
      cout << hex << "TARGET: " << R_TARGET_2 << endl;
      cout << hex << "SIZE  : " << R_SIZE_2 << endl;
      cout << hex << "START : " << R_START_2 << endl;
#endif
      t_addr_temp = R_TARGET_2;
      s_addr_temp = R_SOURCE_2;
      // DMA read from source
      if(R_START_2 && rw_m == READ){
#ifdef debug1
	      cout << "--- DMA read data from source memory ! ---" << endl;
#endif         
	      cmd = tlm::TLM_READ_COMMAND;
        trans->set_command(cmd);
        trans->set_address(s_addr_temp);
        trans->set_data_ptr(reinterpret_cast<unsigned char*> (&data_m));
        trans->set_data_length(1);
        trans->set_streaming_width(1);
        trans->set_byte_enable_ptr(0);
        trans->set_dmi_allowed(false);
        trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
        // Sent to DMA slave port
        master_port->b_transport(*trans, delay);

        //reg_m = trans.get_data_ptr();
        //data_m = *reg_m;
        rw_m = WRITE;
#ifdef debug1
        cout << hex  << "SOURCE: " << R_SOURCE_2 << endl;
        cout << hex  << "TARGET: " << R_TARGET_2 << endl;
        cout << hex  << "SIZE  : " << R_SIZE_2   << endl;
        cout << hex  << "START : " << R_START_2  << endl;
        cout << "data  : " << reinterpret_cast<unsigned int*> (&data_m) << endl;
      	cout << "---------------" << endl;
#endif
      	break;
      }
      // DMA write to target
      else if(R_START_2 && rw_m == WRITE){
#ifdef debug1
 	      cout << "--- DMA write data to target memory ! ---" << endl;
#endif
	      cmd = tlm::TLM_WRITE_COMMAND;
        trans->set_command(cmd);
        trans->set_address(t_addr_temp);
        trans->set_data_ptr(reinterpret_cast<unsigned char*> (&data_m));
        trans->set_data_length(1);
        trans->set_streaming_width(1);
        trans->set_byte_enable_ptr(0);
        trans->set_dmi_allowed(false);
        trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
        // Sent to DMA slave port
        master_port->b_transport(*trans, delay);
        
       	R_SOURCE_2 = R_SOURCE_2 + 1;
        R_TARGET_2 = R_TARGET_2 + 1;
        rw_m = READ;
        R_SIZE_2 -= 1;
#ifdef debug1
        cout << hex << "SOURCE: " << R_SOURCE_2 << endl;
        cout << hex << "TARGET: " << R_TARGET_2 << endl;
        cout << hex << "SIZE  : " << R_SIZE_2 << endl;
        cout << hex << "START : " << R_START_2 << endl;
        cout << "data  : " << reinterpret_cast<unsigned int*> (&data_m) << endl;
	      cout << "---------------" << endl;
#endif
	      break;
      }
    }// close while(R_START == 1 && R_SIZE != 0)

    if(R_SIZE_2 == 0 && R_START_2 == 1){
#ifdef debug
      cout << "---- Interrupt CPU RISCV2 ----" << endl;
#endif
      R_START_2 = 0;
      ir.write(1);
      ir2.write(1);
    }

    // Clear REG write from plaform 
    if(ir.read() == 1 && R_CLEAR_2 == 1) {
#ifdef debug
      cout << "----RISCV2 Clear DMA ----" << endl;
#endif
      ir.write(0);
      ir2.write(0);
      R_SOURCE_2 = 0;
      R_TARGET_2 = 0;
      R_SIZE_2   = 0;
      R_START_2  = 0;
      R_CLEAR_2  = 0;
    }

   }// close channel == 1

    wait();
  }//close while(1)



}//close dma()
