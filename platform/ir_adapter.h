#ifndef IR_ADAPTER_H
#define IR_ADAPTER_H

#include "systemc.h"
#include "tlm.h"
//#include "tlm_utils/simple_target_socket.h"
//#include "tlm_utils/simple_initiator_socket.h"

using namespace sc_core;
using namespace sc_dt;
using namespace std;

SC_MODULE(IR_ADAPTER){
  sc_in<bool> ir_dma;
  
  tlm::tlm_analysis_port<unsigned int> ir;

  void ir_adapter();

  SC_CTOR(IR_ADAPTER){
  	SC_METHOD(ir_adapter);
	sensitive << ir_dma;
  }


};



#endif

