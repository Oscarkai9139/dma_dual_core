/*
 *
 * Copyright (c) 2005-2019 Imperas Software Ltd., www.imperas.com
 *
 * The contents of this file are provided under the Software License
 * Agreement that you accepted before downloading this file.
 *
 * This source forms part of the Software and can be used for educational,
 * training, and demonstration purposes but cannot be used for derivative
 * works except in cases where the derivative works require OVP technology
 * to run.
 *
 * For open source models released under licenses that you can use for
 * derivative works, please visit www.OVPworld.org or www.imperas.com
 * for the location of the open source models.
 *
 */

#include "ovpworld.org/modelSupport/tlmModule/1.0/tlm/tlmModule.hpp"
#include "ovpworld.org/modelSupport/tlmDecoder/1.0/tlm/tlmDecoder.hpp"
#include "ovpworld.org/memory/ram/1.0/tlm/tlmMemory.hpp"
//#include "ovpworld.org/processor/or1k/1.0/tlm/processor.igen.hpp"
#include "andes.ovpworld.org/processor/riscv/1.0/tlm/processor.igen.hpp"
#include "dma.h"
#include "ir_adapter.h"

//#define waveform

// alternative processors:
// #include "arm.ovpworld.org/processor/arm/1.0/tlm/processor.igen.hpp"
// #include "mips.ovpworld.org/processor/mips32/1.0/tlm/processor.igen.hpp"

using namespace sc_core;

////////////////////////////////////////////////////////////////////////////////
//                      BareMetal Class                                       //
////////////////////////////////////////////////////////////////////////////////

class BareMetal : public sc_module {

public:
    BareMetal (sc_module_name name);
    sc_in <bool>          clk;
    sc_in <bool>          rst;
    sc_signal<bool>       ir_wire;
    tlmModule             Platform;
    tlmDecoder            bus1;
    tlmRam                ram1;
    tlmRam                ram2;
    riscv                 cpu1;
    DMA*                  dma1;
    IR_ADAPTER*           iradapter1;

    // alternative processors:
    // arm                   cpu1;
    // mips32                cpu1;

private:

    params paramsForcpu1() {
        params p;
        p.set("defaultsemihost", true);
        return p;
    }

}; /* BareMetal */

BareMetal::BareMetal (sc_module_name name)
    : sc_module (name)
    , Platform ("")
    , bus1 (Platform, "bus1", 3, 3)
    , ram1 (Platform, "ram1", 0x2fffffff)
    , ram2 (Platform, "ram2", 0x2fffffff)
    , cpu1 (Platform, "cpu1",  paramsForcpu1())
{

    dma1 = new DMA("DMA1");
    iradapter1 = new IR_ADAPTER("IR_ADAPTER1");
    dma1->clk(clk);
    dma1->rst(rst);
    cout << "setting up hardware..." << endl;

    cpu1.INSTRUCTION.socket(bus1.target_socket[0]);
    cpu1.DATA.socket(bus1.target_socket[1]);
    dma1->master_port(bus1.target_socket[2]);
    
    //interrupt adapter connection
    dma1->ir(ir_wire);
    iradapter1->ir_dma(ir_wire);
    iradapter1->ir(cpu1.MExternalInterrupt);
	
    bus1.initiator_socket[0](ram1.sp1);
    bus1.initiator_socket[1](ram2.sp1);
    bus1.initiator_socket[2](dma1->slave_port);	
	
    bus1.setDecode(0,0x00000000,0x2fffffff);	// ram1 range
    bus1.setDecode(1,0xd0000000,0xffffffff);	// ram2 range
    bus1.setDecode(2,0xa0000000,0xafffffff);	// dma range

/*
    bus1.connect(cpu1.INSTRUCTION);
    bus1.connect(cpu1.DATA);
    bus1.connect(ram1.sp1, 0x0, 0xffffffff);
*/
}

int sc_main (int argc, char *argv[]) {

    sc_time clkprd(10,SC_NS),clkDly(0,SC_NS);
    sc_clock clk("clk",clkprd,0.50,clkDly,false);
    sc_signal<bool> rst;

    // start the CpuManager session
    session s;

    // create a standard command parser and parse the command line
    parser  p(argc, (const char**) argv);

    // create an instance of the platform
    BareMetal top ("top");

    top.clk(clk);
    top.rst(rst);
    rst=true;
    
    // Set wave signal name
#ifdef waveform
    sc_trace_file *tf = sc_create_vcd_trace_file("RESULT");
    sc_trace(tf, clk, "clock");
    sc_trace(tf, rst, "reset");
    sc_trace(tf, top.dma1->R_SOURCE, "SOURCE");
    sc_trace(tf, top.dma1->R_TARGET, "TARGET");
    sc_trace(tf, top.dma1->R_SIZE,   "SIZE");
    sc_trace(tf, top.dma1->R_START,  "START");
    sc_trace(tf, top.dma1->s_addr_temp, "s_addr_temp");
    sc_trace(tf, top.dma1->t_addr_temp, "t_addr_temp");
    sc_trace(tf, top.dma1->data_m,      "data_m");
    sc_trace(tf, top.dma1->rw_m,        "rw_m");
#endif

    // start SystemC
    sc_start();
    //sc_start(0, SC_NS);
    //rst.write(0);
    //sc_start(clkprd);
    //rst.write(1);
    //sc_start(clkprd);
    //rst.write(0);

    return 0;
}

