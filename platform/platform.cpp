/*
 *
 * Copyright (c) 2005-2019 Imperas Software Ltd., www.imperas.com
 *
 * The contents of this file are provided under the Software License
 * Agreement that you accepted before downloading this file.
 *
 * This source forms part of the Software and can be used for educational,
 * training, and demonstration purposes but cannot be used for derivative
 * works except in cases where the derivative works require OVP technology
 * to run.
 *
 * For open source models released under licenses that you can use for
 * derivative works, please visit www.OVPworld.org or www.imperas.com
 * for the location of the open source models.
 *
 */

#include "ovpworld.org/modelSupport/tlmModule/1.0/tlm/tlmModule.hpp"
#include "ovpworld.org/modelSupport/tlmDecoder/1.0/tlm/tlmDecoder.hpp"
#include "ovpworld.org/memory/ram/1.0/tlm/tlmMemory.hpp"
#include "andes.ovpworld.org/processor/riscv/1.0/tlm/processor.igen.hpp"

#include "dma.h"
#include "ir_adapter.h"


// alternative processors:
// #include "arm.ovpworld.org/processor/arm/1.0/tlm/processor.igen.hpp"
// #include "mips.ovpworld.org/processor/mips32/1.0/tlm/processor.igen.hpp"

using namespace sc_core;

////////////////////////////////////////////////////////////////////////////////
//                      BareMetal Class                                       //
////////////////////////////////////////////////////////////////////////////////

class BareMetal : public sc_module {

  public:
    BareMetal (sc_module_name name);
    sc_in <bool>          clk;
    sc_in <bool>          rst;
    sc_signal <bool>      ir_wire;
    sc_signal <bool>      ir_wire_2;
    tlmModule             Platform;
    tlmDecoder            bus1;
    tlmDecoder            bus2;
    tlmDecoder            bus3;
    tlmRam                ram1;
    tlmRam                ram2;
    tlmRam                ram3;
    tlmRam                ram4;
    tlmRam                ram5;
    tlmRam                ram6;
    riscv                 cpu1;
    riscv                 cpu2;
    DMA*                   dma1;
    IR_ADAPTER*            iradapter1;
    IR_ADAPTER*            iradapter2;
    //sc_in<bool> clk,reset;
    //DMA_tlm       *dma1;
    // alternative processors:
    // arm                   cpu1;
    // mips32                cpu1;

  private:

    params paramsForcpu1() {
        params p;
        p.set("defaultsemihost", true);
        return p;
    }
    params paramsForcpu2() {
        params p;
        p.set("defaultsemihost", true);
        return p;
    }

}; /* BareMetal */

BareMetal::BareMetal (sc_module_name name)
    : sc_module (name)
    , Platform ("")
    , bus1 (Platform, "bus1", 3, 3)
    , bus2 (Platform, "bus2", 2, 3)
    , bus3 (Platform, "bus3", 2, 3)
    , ram1 (Platform, "ram1", 0x00ffffff)//shared ram 1
    , ram2 (Platform, "ram2", 0x00ffffff)//cpu1 local low
    , ram3 (Platform, "ram3", 0x00ffffff)//cpu1 local high
    , ram4 (Platform, "ram4", 0x00ffffff)//cpu2 local low
    , ram5 (Platform, "ram5", 0x00ffffff)//cpu2 local high
    , ram6 (Platform, "ram6", 0x00ffffff)//shared ram 2
    , cpu1 (Platform, "cpu1",  paramsForcpu1())
    , cpu2 (Platform, "cpu2",  paramsForcpu2())
{

    cout << "Setting up Hardware ........." << endl;

// DMA 
    dma1 = new DMA("DMA1");
    iradapter1 = new IR_ADAPTER("IR_ADAPTER1");
    iradapter2 = new IR_ADAPTER("IR_ADAPTER2");
    dma1->clk(clk);
    dma1->rst(rst);

    dma1->ir(ir_wire);
    dma1->ir2(ir_wire_2);
    iradapter1->ir_dma(ir_wire);
    iradapter1->ir(cpu1.MExternalInterrupt);
    iradapter2->ir_dma(ir_wire_2);
    iradapter2->ir(cpu2.MExternalInterrupt);
    dma1->master_port(bus1.target_socket[2]);

//local bus
    cpu1.INSTRUCTION.socket(bus2.target_socket[0]);
    cpu1.DATA.socket(bus2.target_socket[1]);
    cpu2.INSTRUCTION.socket(bus3.target_socket[0]);
    cpu2.DATA.socket(bus3.target_socket[1]);

    bus2.initiator_socket[0]( ram2.sp1 );
    bus2.setDecode(0,0x00000000,0x10ffffff);
    bus2.initiator_socket[1]( ram3.sp1 );
    bus2.setDecode(1,0xff000000,0xffffffff);

    bus3.initiator_socket[0]( ram4.sp1 );
    bus3.setDecode(0,0x00000000,0x10ffffff);
    bus3.initiator_socket[1]( ram5.sp1 );
    bus3.setDecode(1,0xff000000,0xffffffff);

//global bus
    bus2.initiator_socket[2]( bus1.target_socket[0] );
    bus2.setDecode(2,0x11000000,0xfeffffff);
    bus3.initiator_socket[2]( bus1.target_socket[1] );
    bus3.setDecode(2,0x11000000,0xfeffffff);

    bus1.initiator_socket[0]( ram1.sp1 );
    bus1.initiator_socket[1]( ram6.sp1 );
    bus1.initiator_socket[2]( dma1->slave_port );

    bus1.setDecode(0,0xb0000000,0xbfffffff);
    bus1.setDecode(1,0xc0000000,0xcfffffff);
    bus1.setDecode(2,0xa0000000,0xafffffff);
}

int sc_main (int argc, char *argv[]) {
    sc_time clkprd(10, SC_NS), clkDly(0, SC_NS);
    sc_clock clk("clk", clkprd, 0.5, clkDly, false);
    sc_signal <bool> rst;
    //sc_signal<bool>reset;
    // start the CpuManager session
    session s;

    // create a standard command parser and parse the command line
    parser  p(argc, (const char**) argv);

    // create an instance of the platform
    BareMetal top ("top");
    top.clk(clk);
    top.rst(rst);

    //top.clk(clk);
    //top.reset(reset);  
    // start SystemC
    sc_start();
    return 0;
}

