#!/bin/sh

# Check Installation supports this example
checkinstall.exe -p install.pkg --nobanner || exit

# Select CrossCompiler for OR1K/or1k
CROSS=RISCV32
TARGET1=risc1_proj
TARGET2=risc2_proj

# Build Application
make -C application CROSS=${CROSS} TARGET=risc1_proj
make -C application CROSS=${CROSS} TARGET=risc2_proj

# Build Platform
make -C platform

platform/platform.${IMPERAS_ARCH}.exe -program top.cpu1=application/${TARGET2}.${CROSS}.elf \
                                      -program top.cpu2=application/${TARGET1}.${CROSS}.elf -mpdegui

