# ESL System Design Final project
[link](https://hackmd.io/@Ming1116/ryzAJmcAI)

## System Architecture 
![](https://i.imgur.com/8KJL8pa.jpg)
* RISCV1 CPU 
    * reside on BUS2
    * RAM2 local data memory
    * RAM3 local instruction memory
* RISCV2 CPU
    * reisde on BUS3 
    * RAM4 local data memory
    * RAM5 local instruction memory
* Common Bus1
    * Dual channel DMA 
    * RAM1 shared memory
    * RAM6 shared memory

## Program flow
![](https://i.imgur.com/zNRYHrM.jpg)
* Two state for each RISCV cpu to implement a synchronous system
    * READ state (IDLE)
        * In this state, cpu will keep checking the value at 0x200800
        * If the memory value is 0, means RISCV1 has the permission to config DMA, while 1 means RISCV2 has the permission to config DMA.
    * config DMA (BUSY)
        * In this state, RISCV cpu will config DMA with its channel.
        * After finish configuration, RISCV cpu will back to idle state.

## Dual channel DMA design
![](https://i.imgur.com/fW7a9iC.jpg)
* A dual channel DMA
    * two set of control registers
    * channel 1 for RISCV1
    * channel 2 for RISCV2